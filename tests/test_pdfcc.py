"""The module containts tests for PdfCC class.
"""
import os
import sys
# from file_wizard.file_manager import FileManager
from pdfcc.pdfcc import Pdf
from pdfcc.pdfcc import PdfCC


def testPdfBottomMarginNegative(caplog):
    """Checks for negative margin.
    """
    pdf = Pdf()
    message = ("The bottom margin should be set to -1 or should be "
               "greater than 0. Using the default value for the bottom "
               "margin")
    pdf.setPageBoundary(bottomMargin=-10)
    receivedMessage = caplog.text
    pageBoundary = pdf.getPageBoundary()
    assert message in receivedMessage and pageBoundary["bottomMargin"] == -1


def testPdfBottomTopMismatch(caplog):
    """Checks for mismatch between the bottom and top values.
    """
    pdf = Pdf()
    message = ("The bottom margin should be  greater than top margin.")
    pdf.setPageBoundary(topMargin=50, bottomMargin=10)
    receivedMessage = caplog.text
    pageBoundary = pdf.getPageBoundary()
    assert message in receivedMessage and pageBoundary["bottomMargin"] == -1


def testPdfTopMarginNegative(caplog):
    """Checks for negative margin.
    """
    pdf = Pdf()
    message = ("The given value for top Margin is lesser than zero. "
               "Using the default value for the top margin")
    pdf.setPageBoundary(topMargin=-10)
    receivedMessage = caplog.text
    pageBoundary = pdf.getPageBoundary()
    assert message in receivedMessage and pageBoundary["topMargin"] == 0


def testPdfLeftMarginNegative(caplog):
    """Checks for negative margin.
    """
    pdf = Pdf()
    message = ("The given value for left Margin is lesser than zero. "
               "Using the default value for the left margin")
    pdf.setPageBoundary(leftMargin=-10)
    receivedMessage = caplog.text
    pageBoundary = pdf.getPageBoundary()
    assert message in receivedMessage and pageBoundary["leftMargin"] == 0


def testPdfRightMarginNegative(caplog):
    """Checks for negative margin.
    """
    pdf = Pdf()
    message = ("The right margin should be set to -1 or should be "
               "greater than 0. Using the default value for the right "
               "margin")
    pdf.setPageBoundary(rightMargin=-10)
    receivedMessage = caplog.text
    pageBoundary = pdf.getPageBoundary()
    assert message in receivedMessage and pageBoundary["rightMargin"] == -1


def testPdfRightLeftMismatch(caplog):
    """Checks for mismatch between the right and left margins.
    """
    pdf = Pdf()
    message = ("The right margin should be greater than left margin.")
    pdf.setPageBoundary(rightMargin=10, leftMargin=40)
    receivedMessage = caplog.text
    pageBoundary = pdf.getPageBoundary()
    assert message in receivedMessage and pageBoundary["rightMargin"] == -1


def testPdfccFileMissing(caplog):
    """Tests the behaviour of PdfCC when input is missing.
    """
    pdf = PdfCC("missing.pdf", "output.pdf")
    pdf.setPageBoundary(leftMargin=0,
                        rightMargin=100,
                        topMargin=0,
                        bottomMargin=100)
    status = pdf.cropFile()
    assert status is False and "File does not exist." in caplog.text


def testPdfccIncorrectFile(caplog):
    """Tests the behaviour of PdfCC when input file cannot be read.
    """
    pdf = PdfCC(os.path.join(sys.path[0], "data/incorrect.pdf"), "output.pdf")
    pdf.setPageBoundary(leftMargin=0,
                        rightMargin=100,
                        topMargin=0,
                        bottomMargin=100)
    status = pdf.cropFile()
    assert status is False and "File type not recognised." in caplog.text


def testPdfccComplete(tmpdir):
    """Tests the behaviour of PdfCC when input file cannot be read.
    """
    pdf = PdfCC(os.path.join(sys.path[0], "data/test1.pdf"),
                os.path.join(tmpdir, "output.pdf"))
    pdf.setPageBoundary(leftMargin=100,
                        rightMargin=200,
                        topMargin=100,
                        bottomMargin=200)
    status = pdf.cropFile()
    assert status is True
    assert os.path.exists(os.path.join(tmpdir, "output.pdf")) is True


def testPdfccCompleteWrongMargin(tmpdir):
    """Tests the behaviour of PdfCC when input file cannot be read.
    """
    pdf = PdfCC(os.path.join(sys.path[0], "data/test2.pdf"),
                os.path.join(tmpdir, "output.pdf"))
    pdf.setPageBoundary(leftMargin=100,
                        rightMargin=20000,
                        topMargin=100,
                        bottomMargin=200)
    status = pdf.cropFile()
    assert status is True
    assert os.path.exists(os.path.join(tmpdir, "output.pdf")) is True
