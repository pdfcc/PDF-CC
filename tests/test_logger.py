"""Module contains functions for testing the logger module.
"""
import logging
from pdfcc import logger


def testLoggerConsole(capfd):
    """Function checks whether the console logging is working.
    """
    log: logging.Logger = logger.initLogger()
    message: str = "Hello world"
    log.debug(message)
    captured = capfd.readouterr()
    assert message in captured.err


def testLoggerConsoleDisable(capfd):
    """Function checks whether the console logging is working.
    """
    log: logging.Logger = logger.initLogger(verbose=False)
    message: str = "Hello world"
    log.debug(message)
    captured = capfd.readouterr()
    assert message not in captured.err


def testLoggerFile(caplog):
    """Function checks whether the file logging is working.
    """
    log: logging.Logger = logger.initLogger()
    message: str = "Hello world"
    log.setLevel(logging.DEBUG)
    log.debug(message)
    txt: str = caplog.text
    assert message in txt


def testLoggerFileDisable():
    """Function checks whether the file logging is working.
    """
    log: logging.Logger = logger.initLogger(logfile=False)
    for handler in log.handlers:
        assert isinstance(handler, logging.FileHandler) is False


def testLoggerDisable():
    """Function checks whether the file logging is working.
    """
    log: logging.Logger = logger.initLogger(logfile=False, verbose=False)
    for handler in log.handlers:
        assert isinstance(handler, logging.FileHandler) is False
        assert isinstance(handler, logging.StreamHandler) is False
