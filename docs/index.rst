.. PdfCC documentation master file, created by
   sphinx-quickstart on Sat Nov 21 00:16:24 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PdfCC's documentation!
=================================

.. toctree::
   :maxdepth: 5
   :caption: Contents:
   
   Introduction <readme>
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
