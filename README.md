PDF Crop & Compress

A libarary to crop boundaries from pdf files, removing unwanted colours and preserves relevant text. Also compresses pdf size.

<h2> Testing Reports </h2>
See [unit testing report](https://pdfcc.gitlab.io/unit)<br>
See testing [coverage report](https://pdfcc.gitlab.io/cov/)<br>
See [documentation](https://pdfcc.gitlab.io/doc/)
